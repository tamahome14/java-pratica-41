
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author jorge
 */
public class Pratica41 {
    public static void main(String[] args) {
        Circulo circulo = new Circulo(10);
        Elipse elipse = new Elipse(4,2);
        System.out.println(circulo.getPerimetro());
        System.out.println(circulo.getArea());
        System.out.println(elipse.getPerimetro());
        System.out.println(elipse.getArea());
    }    
}
